select (location = null), time, item, sum(quantity)
 from sales
 group by time, item
 union
     select location, (time = null), item, sum(quantity)
 from sales
 group by location, item
 union
 select location, time, (item = null), sum(quantity)
 from sales
 group by location, time
 union
 select (location = null), (time = null), item, sum(quantity)
 from sales
 group by item
 union
 select (location = null), time, (item = null), sum(quantity)
 from sales
 group by time
     union
     select location, (time = null), (item = null), sum(quantity)
 from sales
 group by location
 union
 select (location = null), (time = null), (item = null), sum(quantity)
 from sales
 union
 select location, time, item, sum(quantity)
 from sales
 group by location, time, item;